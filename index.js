const TelegramApi = require("node-telegram-bot-api");
const mongoose = require("mongoose");
const User = require("./models/user");
const Log = require("./models/log");
const express = require("express");
const csvParser = require("csv-parser");
const token = "5910482800:AAGs1e3q3_vMTBQMHkYRHRc0MAgftzWntFI";
const host = "192.168.0.105";
const port = 3000;
const fs = require("fs");
const bot = new TelegramApi(token, { polling: true });
const app = express();
const db =
  "mongodb+srv://admin:d4v6fnR3@main.ejfkfz6.mongodb.net/users?retryWrites=true&w=majority";

// Connection to DB
mongoose
  .connect(db, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    logEvent("info", "Успішно з'єднано з БД.");
  })
  .catch((e) => {
    logEvent("error", `Помилка підключення: ${e}`);
  });

app.use(express.json());
// Post handler function
app.post("/uid", function (req, res) {
  console.log(req.body);
  User.findOne({ uid: req.body.uid }, (err, us) => {
    if (us !== null) {
      bot.sendMessage(
        182888616,
        `<b>Доступ дозволено для:</b>\n<u>UID:</u> ${us.uid}\n<u>Ім\'я:</u> ${
          us.firstName
        }\n<u>Прізвище:</u> ${us.lastName}\n<u>Посада:</u> ${
          us.role
        }\n<i>Дата доступу: ${new Date().toLocaleString("ua-UA", {})}</i>`,
        { parse_mode: "HTML" }
      );
      logEvent(
        "accessGranted",
        `Доступ користувачеві / ${us.uid} ${us.firstName} ${us.lastName} / дозволено. Замок відкритий!`
      );
      res.status(200).json({ message: "Доступ дозволено!" });
    } else {
      bot.sendMessage(
        182888616,
        "Такого запису не існує! Відмовлено у доступі!"
      );
      logEvent(
        "accessDenied",
        `Доступ користувачеві з UID / ${req.body.uid} / заборонено.`
      );
      res.status(404).json({ message: "Доступ заборонено!" });
    }
  });
});
//Start server function
app.listen(port, host, () => {
  console.log(`Server started on ${host}:${port}`);
  logEvent("info", "Сервер запущено.");
});

//Logging function
const logEvent = async (logType, logText) => {
  const log = new Log({
    logId: Math.random() * 10e15,
    type: logType,
    text: logText,
    date: new Date().toLocaleString("ua-UA", {}),
  });
  await log
    .save()
    .then()
    .catch((e) => {
      console.error(`Error ${e} ocurred while logging!`);
    });
};
//Save user function
const saveUser = (user, chatId) => {
  User.findOne({ uid: user.uid }, async (err, item) => {
    if (item === null) {
      await user
        .save()
        .then(() => {
          logEvent("info", `Користувача з UID ${user.uid} успішно створено.`);
          return bot.sendMessage(
            chatId,
            `✅ Користувача з UID ${user.uid} успішно створено!`
          );
        })
        .catch((e) => {
          console.error(e);
          logEvent("error", "При створенні користувача виникла помилка.");
        });
    } else {
      return bot.sendMessage(
        chatId,
        "❌ Помилка створення: користувач з таким UID вже існує!"
      );
    }
  });
};
//Get all users function
const getAllUsers = async () => {
  const users = await User.find().sort({ $natural: -1 }).limit(50);
  let res = "👥 Список користувачів останні 50 записів): \n";
  users.forEach((el) => {
    res += "| ";
    res += el.uid + " | ";
    res += el.firstName + "  ";
    res += el.lastName + "  ";
    res += el.role + " ";
    res += "\n";
  });
  return res;
};
//Export all users function
const exportAllUsers = async () => {
  const usersList = await User.find();
  let res = "";
  usersList.forEach((el) => {
    res += el.uid + ";";
    res += el.firstName + ";";
    res += el.lastName + ";";
    res += el.role + "\n";
  });
  let fileName = "exports/users.csv";
  fs.writeFile(fileName, res, { encoding: "utf-8", flag: "w" }, (err) => {
    if (err) return;
    else console.log("Users saved");
  });
};
//Export all logs function
const exportAllLogs = async () => {
  const logList = await Log.find();
  let res = "Log ID;Log type;Text;Date\n";
  logList.forEach((el) => {
    res += el.logId + ";";
    res += el.type + ";";
    res += el.text + ";";
    res += el.date + "\n";
  });
  let fileName = "exports/logs.csv";
  fs.writeFile(fileName, res, { flag: "w" }, (err) => {
    if (err) return;
    else {
      console.log("Logs saved");
      return;
    }
  });
};
// Get logs function
const getLogs = async () => {
  const logs = await Log.find().sort({ $natural: -1 }).limit(25);
  let list = "📑 Список останніх 25 подій: \n";
  logs.forEach((el) => {
    list += "-  ";
    list += el.text + "  ";
    list += el.date + "  ";
    list += "\n";
  });
  return list;
};
// Add user with bot handler
bot.onText(/\/add (.+)?/, function (msg, match) {
  let str = match[1];
  if (str != null) {
    const data = str.split(";");
    const newUser = new User({
      uid: data[0],
      firstName: data[1],
      lastName: data[2],
      role: data[3],
    });
    saveUser(newUser, msg.chat.id);
  } else console.log("null");
});
// Delete user with bot handler
bot.onText(/\/rm (.+)?/, async function (msg, match) {
  let str = match[1];
  if (str != null) {
    User.findOne({ uid: str }, async (err, usr) => {
      if (usr !== null) {
        const res = await User.deleteOne({ uid: str });
        logEvent("userDeleted", `Користувача з UID ${str} успішно видалено.`);
        return bot.sendMessage(msg.chat.id, "🚮 Користувача успішно видалено!");
      } else
        return bot.sendMessage(msg.chat.id, "❌ Такого користувача не існує!");
    });
  } else return bot.sendMessage(chatId, "❌ Команда rm має містити номер UID в якості параметра (/rm uid)!");
});
// Setting commands
bot.setMyCommands([
  { command: "/start", description: "Розпочати роботу бота." },
  { command: "/users", description: "Список користувачів." },
  { command: "/logs", description: "Журнал подій." },
  {
    command: "/exportusers",
    description: "Експорт списку користувачів в CSV.",
  },
  { command: "/exportlogs", description: "Експорт журналу подій в CSV." },
]);
//Bot messages handler
bot.on("message", async (msg) => {
  const text = msg.text;
  const chatId = msg.chat.id;
  if (text === "/start") {
    await bot.sendMessage(chatId, "Бот успішно запущений!");
  }
  if (text === "/users") {
    const list = await getAllUsers();
    return bot.sendMessage(chatId, list);
  }
  if (text === "/logs") {
    const logs = await getLogs();
    return bot.sendMessage(chatId, logs);
  }
  if (text === "/clearusers") {
    User.deleteMany({}).then(() => {
      bot.sendMessage(chatId, "Users collection cleared!");
    });
  }
  if (text === "/clearlogs") {
    Log.deleteMany({}).then(() => {
      bot.sendMessage(chatId, "Logs collection cleared!");
    });
  }
  if (text === "/exportusers") {
    exportAllUsers(chatId);
    await bot.sendDocument(chatId, "exports/users.csv", {
      caption: `Список користувачів на: \n${new Date().toLocaleString(
        "ua-UA",
        {}
      )}`,
    });
  }
  if (text === "/exportlogs") {
    exportAllLogs(chatId);
    await bot.sendDocument(chatId, "exports/logs.csv", {
      caption: `Список логів на: \n${new Date().toLocaleString("ua-UA", {})}`,
    });
  }
});

//Bot csv file handler
bot.on("document", async (msg) => {
  if (msg.document.mime_type === "text/csv") {
    const fileId = msg.document.file_id;
    let file = "";
    const downloadDir = "downloads/";
    await bot.downloadFile(fileId, downloadDir).then((fileName) => {
      file = fileName;
      bot.sendMessage(msg.chat.id, `🔍 Файл обробляється, зачекайте...`);
    });

    const res = [];
    fs.createReadStream(file.replace("\\", "/"))
      .pipe(
        csvParser({
          separator: ";",
          headers: ["UID", "Name", "Surname", "Role"],
        })
      )
      .on("data", (data) => {
        console.log(data);
        const tmpUser = new User({
          uid: data.UID,
          firstName: data.Name,
          lastName: data.Surname,
          role: data.Role,
        });
        saveUser(tmpUser, msg.chat.id);
        res.push(data);
      })
      .on("end", () => {
        fs.unlinkSync(file);
      });
  } else
    return bot.sendMessage(
      msg.chat.id,
      "❌ Помилка! Даний бот працює лише з CSV файлами!"
    );
});
