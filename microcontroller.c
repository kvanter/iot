#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <SPI.h>
#include <MFRC522.h>
#define RST_PIN 5
#define SS_PIN 4
#define LOCK_PIN 14
MFRC522 mfrc522(SS_PIN, RST_PIN);
const char* ssid = "******";
const char* password = "********";
const char* serverName = "http://192.168.0.105:3000/uid";

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  while(WiFi.status() != WL_CONNECTED) {
    digitalWrite(LED_BUILTIN, HIGH);
    delay(500);
    digitalWrite(LED_BUILTIN, LOW);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println(WiFi.localIP());
  SPI.begin();
  mfrc522.PCD_Init();
  pinMode(LOCK_PIN, OUTPUT);
  Serial.println(F("Ready"));
}
void sendData(int uid){
 if(WiFi.status()== WL_CONNECTED){
      WiFiClient client;
      HTTPClient http;
      http.begin(client, serverName);
      http.addHeader("Content-Type", "application/json");
      String res = "{\"uid\":\""+ String(uid) + "\"}";
      int httpResponseCode = http.POST(res);   
      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);
      if(httpResponseCode == 200){
        digitalWrite(LOCK_PIN, LOW);
        delay(15000);
        digitalWrite(LOCK_PIN, HIGH);
      }else digitalWrite(LOCK_PIN, HIGH);
      http.end();
    }
    else {
      Serial.println("WiFi Disconnected");
    }
}

void loop() {
  if (mfrc522.PICC_IsNewCardPresent() && mfrc522.PICC_ReadCardSerial()) {
    String uid_;
    uid_ += String(mfrc522.uid.uidByte[1]) + String(mfrc522.uid.uidByte[2]) + String( mfrc522.uid.uidByte[3]) + String( mfrc522.uid.uidByte[4]);
    sendData(uid_.toInt());
    delay(500);
  }
  delay(100);
}
