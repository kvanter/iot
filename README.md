# Сервер

Для запуску сервера потрібно локально встановити NodeJS на комп'ютер.
Посилання: https://nodejs.org/en/download
Після чого виконати клонування коду системи в будь-яке зручне місце на диску.

Після клонування перейдіть в директорію проекту та виконайте команду `npm install` для встановлення необхідних залежностей.

Для запуску локального сервера необхідно замінити значення деяких констант:

```javascript
const token = "Вставте_токен_вашого_бота_сюди";
const host = "Адреса_на_якій_працюватиме_сервер";
const port = Порт_на якому_працюватиме_сервер;
```

В MongoDBAtlas створіть базу даних, скопіюйте рядок підключення та вставте його в наступну константу:
`const db =  "Рядок_підключення_до_БД";`

Відео як створити MongoDB Atlas безкоштовно в хмарі та як отримати рядок підключення: https://www.youtube.com/watch?v=xrc7dIO_tXk

Після встановлення всіх змінних потрібно виконати команду `npm run start` для запуску додатку:

# Мікроконтролер

Для успішного запуску мікроконтролера необхідно встановити наступні бібліотеки:

```c
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include <SPI.h>
#include <MFRC522.h>
```

Також необхідно визначити номери виходів мікроконтролера:

```c
#define RST_PIN 5
#define SS_PIN 4
#define LOCK_PIN 14
```

Також необхідно вказати значення змінних імені мережі, паролю та адресу сервера:

```c
const char* ssid = "Мережа";
const char* password = "Пароль";
const char* serverName = "http://адреса_сервера:порт/uid";
```

Ввівши ці значення необхідно завантажити прошивку на мікроконтролер та заупстити локальний сервер, після чого відбудеться з'єднання та запуск системи.
