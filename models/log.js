const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const LogSchema = new Schema({
  logId: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
  date: {
    type: String,
    required: true,
  },
});

const Log = mongoose.model("Log", LogSchema);
module.exports = Log;
